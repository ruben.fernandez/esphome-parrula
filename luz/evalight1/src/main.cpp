// Auto generated code by esphome
// ========== AUTO GENERATED INCLUDE BLOCK BEGIN ===========
#include "esphome.h"
using namespace esphome;
using namespace switch_;
logger::Logger *logger_logger;
web_server_base::WebServerBase *web_server_base_webserverbase;
captive_portal::CaptivePortal *captive_portal_captiveportal;
wifi::WiFiComponent *wifi_wificomponent;
ota::OTAComponent *ota_otacomponent;
api::APIServer *api_apiserver;
using namespace api;
StartupTrigger *startuptrigger;
Automation<> *automation;
gpio::GPIOSwitch *luzsalon1;
switch_::TurnOnAction<> *switch__turnonaction;
// ========== AUTO GENERATED INCLUDE BLOCK END ==========="

void setup() {
  // ===== DO NOT EDIT ANYTHING BELOW THIS LINE =====
  // ========== AUTO GENERATED CODE BEGIN ===========
  // async_tcp:
  // esphome:
  //   name: evalight1
  //   platform: ESP8266
  //   board: nodemcu
  //   on_boot:
  //   - then:
  //     - switch.turn_on:
  //         id: luzsalon1
  //       type_id: switch__turnonaction
  //     automation_id: automation
  //     trigger_id: startuptrigger
  //     priority: 600.0
  //   arduino_version: espressif8266@2.6.2
  //   build_path: evalight1
  //   platformio_options: {}
  //   esp8266_restore_from_flash: false
  //   board_flash_mode: dout
  //   includes: []
  //   libraries: []
  App.pre_setup("evalight1", __DATE__ ", " __TIME__);
  // switch:
  // logger:
  //   id: logger_logger
  //   baud_rate: 115200
  //   tx_buffer_size: 512
  //   hardware_uart: UART0
  //   level: DEBUG
  //   logs: {}
  //   esp8266_store_log_strings_in_flash: true
  logger_logger = new logger::Logger(115200, 512, logger::UART_SELECTION_UART0);
  logger_logger->pre_setup();
  App.register_component(logger_logger);
  // web_server_base:
  //   id: web_server_base_webserverbase
  web_server_base_webserverbase = new web_server_base::WebServerBase();
  App.register_component(web_server_base_webserverbase);
  // captive_portal:
  //   id: captive_portal_captiveportal
  //   web_server_base_id: web_server_base_webserverbase
  captive_portal_captiveportal = new captive_portal::CaptivePortal(web_server_base_webserverbase);
  App.register_component(captive_portal_captiveportal);
  // wifi:
  //   ap:
  //     ssid: Evalight1 Fallback Hotspot
  //     password: 9J2GF7D8muWn
  //     id: wifi_wifiap
  //     ap_timeout: 1min
  //   id: wifi_wificomponent
  //   domain: .local
  //   reboot_timeout: 15min
  //   power_save_mode: NONE
  //   fast_connect: false
  //   output_power: 20.0
  //   networks:
  //   - ssid: Mai
  //     password: Merchimola1000
  //     id: wifi_wifiap_2
  //     priority: 0.0
  //   use_address: evalight1.local
  wifi_wificomponent = new wifi::WiFiComponent();
  wifi_wificomponent->set_use_address("evalight1.local");
  wifi::WiFiAP wifi_wifiap_2 = wifi::WiFiAP();
  wifi_wifiap_2.set_ssid("Mai");
  wifi_wifiap_2.set_password("Merchimola1000");
  wifi_wifiap_2.set_priority(0.0f);
  wifi_wificomponent->add_sta(wifi_wifiap_2);
  wifi::WiFiAP wifi_wifiap = wifi::WiFiAP();
  wifi_wifiap.set_ssid("Evalight1 Fallback Hotspot");
  wifi_wifiap.set_password("9J2GF7D8muWn");
  wifi_wificomponent->set_ap(wifi_wifiap);
  wifi_wificomponent->set_ap_timeout(60000);
  wifi_wificomponent->set_reboot_timeout(900000);
  wifi_wificomponent->set_power_save_mode(wifi::WIFI_POWER_SAVE_NONE);
  wifi_wificomponent->set_fast_connect(false);
  wifi_wificomponent->set_output_power(20.0f);
  App.register_component(wifi_wificomponent);
  // ota:
  //   id: ota_otacomponent
  //   safe_mode: true
  //   port: 8266
  //   password: ''
  //   reboot_timeout: 5min
  //   num_attempts: 10
  ota_otacomponent = new ota::OTAComponent();
  ota_otacomponent->set_port(8266);
  ota_otacomponent->set_auth_password("");
  App.register_component(ota_otacomponent);
  if (ota_otacomponent->should_enter_safe_mode(10, 300000)) return;
  // api:
  //   id: api_apiserver
  //   port: 6053
  //   password: ''
  //   reboot_timeout: 15min
  api_apiserver = new api::APIServer();
  App.register_component(api_apiserver);
  api_apiserver->set_port(6053);
  api_apiserver->set_password("");
  api_apiserver->set_reboot_timeout(900000);
  startuptrigger = new StartupTrigger(600.0f);
  App.register_component(startuptrigger);
  automation = new Automation<>(startuptrigger);
  // switch.gpio:
  //   platform: gpio
  //   name: Luz salon
  //   pin:
  //     number: 14
  //     mode: OUTPUT
  //     inverted: false
  //   id: luzsalon1
  //   restore_mode: RESTORE_DEFAULT_OFF
  //   interlock_wait_time: 0ms
  luzsalon1 = new gpio::GPIOSwitch();
  App.register_component(luzsalon1);
  App.register_switch(luzsalon1);
  luzsalon1->set_name("Luz salon");
  switch__turnonaction = new switch_::TurnOnAction<>(luzsalon1);
  luzsalon1->set_pin(new GPIOPin(14, OUTPUT, false));
  luzsalon1->set_restore_mode(gpio::GPIO_SWITCH_RESTORE_DEFAULT_OFF);
  automation->add_actions({switch__turnonaction});
  // =========== AUTO GENERATED CODE END ============
  // ========= YOU CAN EDIT AFTER THIS LINE =========
  App.setup();
}

void loop() {
  App.loop();
}
