// Auto generated code by esphome
// ========== AUTO GENERATED INCLUDE BLOCK BEGIN ===========
#include "esphome.h"
using namespace esphome;
using namespace binary_sensor;
logger::Logger *logger_logger;
web_server_base::WebServerBase *web_server_base_webserverbase;
captive_portal::CaptivePortal *captive_portal_captiveportal;
wifi::WiFiComponent *wifi_wificomponent;
ota::OTAComponent *ota_otacomponent;
api::APIServer *api_apiserver;
using namespace sensor;
using namespace api;
dht::DHT *dht_dht;
gpio::GPIOBinarySensor *gpio_gpiobinarysensor;
gpio::GPIOBinarySensor *gpio_gpiobinarysensor_2;
gpio::GPIOBinarySensor *gpio_gpiobinarysensor_3;
binary_sensor::DelayedOnOffFilter *binary_sensor_delayedonofffilter;
binary_sensor::DelayedOnOffFilter *binary_sensor_delayedonofffilter_2;
sensor::Sensor *sensor_sensor;
sensor::Sensor *sensor_sensor_2;
// ========== AUTO GENERATED INCLUDE BLOCK END ==========="

void setup() {
  // ===== DO NOT EDIT ANYTHING BELOW THIS LINE =====
  // ========== AUTO GENERATED CODE BEGIN ===========
  // async_tcp:
  // esphome:
  //   name: evatable1
  //   platform: ESP8266
  //   board: nodemcu
  //   arduino_version: espressif8266@2.6.2
  //   build_path: evatable1
  //   platformio_options: {}
  //   esp8266_restore_from_flash: false
  //   board_flash_mode: dout
  //   includes: []
  //   libraries: []
  App.pre_setup("evatable1", __DATE__ ", " __TIME__);
  // binary_sensor:
  // logger:
  //   id: logger_logger
  //   baud_rate: 115200
  //   tx_buffer_size: 512
  //   hardware_uart: UART0
  //   level: DEBUG
  //   logs: {}
  //   esp8266_store_log_strings_in_flash: true
  logger_logger = new logger::Logger(115200, 512, logger::UART_SELECTION_UART0);
  logger_logger->pre_setup();
  App.register_component(logger_logger);
  // web_server_base:
  //   id: web_server_base_webserverbase
  web_server_base_webserverbase = new web_server_base::WebServerBase();
  App.register_component(web_server_base_webserverbase);
  // captive_portal:
  //   id: captive_portal_captiveportal
  //   web_server_base_id: web_server_base_webserverbase
  captive_portal_captiveportal = new captive_portal::CaptivePortal(web_server_base_webserverbase);
  App.register_component(captive_portal_captiveportal);
  // wifi:
  //   ap:
  //     ssid: Evatable1 Fallback Hotspot
  //     password: RLzXRFL4UxnM
  //     id: wifi_wifiap
  //     ap_timeout: 1min
  //   id: wifi_wificomponent
  //   domain: .local
  //   reboot_timeout: 15min
  //   power_save_mode: NONE
  //   fast_connect: false
  //   output_power: 20.0
  //   networks:
  //   - ssid: Mai
  //     password: Merchimola1000
  //     id: wifi_wifiap_2
  //     priority: 0.0
  //   use_address: evatable1.local
  wifi_wificomponent = new wifi::WiFiComponent();
  wifi_wificomponent->set_use_address("evatable1.local");
  wifi::WiFiAP wifi_wifiap_2 = wifi::WiFiAP();
  wifi_wifiap_2.set_ssid("Mai");
  wifi_wifiap_2.set_password("Merchimola1000");
  wifi_wifiap_2.set_priority(0.0f);
  wifi_wificomponent->add_sta(wifi_wifiap_2);
  wifi::WiFiAP wifi_wifiap = wifi::WiFiAP();
  wifi_wifiap.set_ssid("Evatable1 Fallback Hotspot");
  wifi_wifiap.set_password("RLzXRFL4UxnM");
  wifi_wificomponent->set_ap(wifi_wifiap);
  wifi_wificomponent->set_ap_timeout(60000);
  wifi_wificomponent->set_reboot_timeout(900000);
  wifi_wificomponent->set_power_save_mode(wifi::WIFI_POWER_SAVE_NONE);
  wifi_wificomponent->set_fast_connect(false);
  wifi_wificomponent->set_output_power(20.0f);
  App.register_component(wifi_wificomponent);
  // ota:
  //   id: ota_otacomponent
  //   safe_mode: true
  //   port: 8266
  //   password: ''
  //   reboot_timeout: 5min
  //   num_attempts: 10
  ota_otacomponent = new ota::OTAComponent();
  ota_otacomponent->set_port(8266);
  ota_otacomponent->set_auth_password("");
  App.register_component(ota_otacomponent);
  if (ota_otacomponent->should_enter_safe_mode(10, 300000)) return;
  // api:
  //   id: api_apiserver
  //   port: 6053
  //   password: ''
  //   reboot_timeout: 15min
  api_apiserver = new api::APIServer();
  App.register_component(api_apiserver);
  // sensor:
  api_apiserver->set_port(6053);
  api_apiserver->set_password("");
  api_apiserver->set_reboot_timeout(900000);
  // sensor.dht:
  //   platform: dht
  //   pin:
  //     number: 4
  //     mode: INPUT
  //     inverted: false
  //   temperature:
  //     name: Temperatura
  //     id: sensor_sensor
  //     force_update: false
  //     unit_of_measurement: °C
  //     icon: mdi:thermometer
  //     accuracy_decimals: 1
  //   humidity:
  //     name: Humedad
  //     id: sensor_sensor_2
  //     force_update: false
  //     unit_of_measurement: '%'
  //     icon: mdi:water-percent
  //     accuracy_decimals: 0
  //   update_interval: 30s
  //   model: DHT11
  //   id: dht_dht
  dht_dht = new dht::DHT();
  dht_dht->set_update_interval(30000);
  App.register_component(dht_dht);
  // binary_sensor.gpio:
  //   platform: gpio
  //   pin:
  //     number: 14
  //     mode: INPUT
  //     inverted: false
  //   name: Sensor
  //   device_class: motion
  //   id: gpio_gpiobinarysensor
  gpio_gpiobinarysensor = new gpio::GPIOBinarySensor();
  App.register_component(gpio_gpiobinarysensor);
  // binary_sensor.gpio:
  //   platform: gpio
  //   pin:
  //     number: 5
  //     mode: INPUT_PULLUP
  //     inverted: false
  //   name: Puerta 1
  //   device_class: door
  //   filters:
  //   - delayed_on_off: 500ms
  //     type_id: binary_sensor_delayedonofffilter
  //   id: gpio_gpiobinarysensor_2
  gpio_gpiobinarysensor_2 = new gpio::GPIOBinarySensor();
  App.register_component(gpio_gpiobinarysensor_2);
  // binary_sensor.gpio:
  //   platform: gpio
  //   pin:
  //     number: 13
  //     mode: INPUT_PULLUP
  //     inverted: false
  //   name: Puerta 2
  //   device_class: door
  //   filters:
  //   - delayed_on_off: 500ms
  //     type_id: binary_sensor_delayedonofffilter_2
  //   id: gpio_gpiobinarysensor_3
  gpio_gpiobinarysensor_3 = new gpio::GPIOBinarySensor();
  App.register_component(gpio_gpiobinarysensor_3);
  App.register_binary_sensor(gpio_gpiobinarysensor);
  gpio_gpiobinarysensor->set_name("Sensor");
  gpio_gpiobinarysensor->set_device_class("motion");
  App.register_binary_sensor(gpio_gpiobinarysensor_2);
  gpio_gpiobinarysensor_2->set_name("Puerta 1");
  gpio_gpiobinarysensor_2->set_device_class("door");
  binary_sensor_delayedonofffilter = new binary_sensor::DelayedOnOffFilter(500);
  App.register_component(binary_sensor_delayedonofffilter);
  App.register_binary_sensor(gpio_gpiobinarysensor_3);
  gpio_gpiobinarysensor_3->set_name("Puerta 2");
  gpio_gpiobinarysensor_3->set_device_class("door");
  binary_sensor_delayedonofffilter_2 = new binary_sensor::DelayedOnOffFilter(500);
  App.register_component(binary_sensor_delayedonofffilter_2);
  dht_dht->set_pin(new GPIOPin(4, INPUT, false));
  sensor_sensor = new sensor::Sensor();
  App.register_sensor(sensor_sensor);
  sensor_sensor->set_name("Temperatura");
  sensor_sensor->set_unit_of_measurement("\302\260C");
  sensor_sensor->set_icon("mdi:thermometer");
  sensor_sensor->set_accuracy_decimals(1);
  sensor_sensor->set_force_update(false);
  gpio_gpiobinarysensor->set_pin(new GPIOPin(14, INPUT, false));
  dht_dht->set_temperature_sensor(sensor_sensor);
  sensor_sensor_2 = new sensor::Sensor();
  App.register_sensor(sensor_sensor_2);
  sensor_sensor_2->set_name("Humedad");
  sensor_sensor_2->set_unit_of_measurement("%");
  sensor_sensor_2->set_icon("mdi:water-percent");
  sensor_sensor_2->set_accuracy_decimals(0);
  sensor_sensor_2->set_force_update(false);
  gpio_gpiobinarysensor_2->add_filters({binary_sensor_delayedonofffilter});
  gpio_gpiobinarysensor_3->add_filters({binary_sensor_delayedonofffilter_2});
  dht_dht->set_humidity_sensor(sensor_sensor_2);
  dht_dht->set_dht_model(dht::DHT_MODEL_DHT11);
  gpio_gpiobinarysensor_2->set_pin(new GPIOPin(5, INPUT_PULLUP, false));
  gpio_gpiobinarysensor_3->set_pin(new GPIOPin(13, INPUT_PULLUP, false));
  // =========== AUTO GENERATED CODE END ============
  // ========= YOU CAN EDIT AFTER THIS LINE =========
  App.setup();
}

void loop() {
  App.loop();
}
