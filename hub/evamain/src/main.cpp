// Auto generated code by esphome
// ========== AUTO GENERATED INCLUDE BLOCK BEGIN ===========
#include "esphome.h"
using namespace esphome;
using namespace binary_sensor;
using namespace switch_;
logger::Logger *logger_logger;
web_server_base::WebServerBase *web_server_base_webserverbase;
captive_portal::CaptivePortal *captive_portal_captiveportal;
wifi::WiFiComponent *wifi_wificomponent;
ota::OTAComponent *ota_otacomponent;
api::APIServer *api_apiserver;
using namespace api;
using namespace spi;
spi::SPIComponent *spi_spicomponent;
rc522_spi::RC522Spi *rc522_spi_rc522spi;
rc522::RC522BinarySensor *rc522_rc522binarysensor;
rc522::RC522BinarySensor *rc522_rc522binarysensor_2;
gpio::GPIOBinarySensor *gpio_gpiobinarysensor;
gpio::GPIOBinarySensor *gpio_gpiobinarysensor_2;
gpio::GPIOBinarySensor *puerta_entrada;
gpio::GPIOSwitch *gpio_gpioswitch;
gpio::GPIOSwitch *gpio_gpioswitch_2;
binary_sensor::DelayedOnOffFilter *binary_sensor_delayedonofffilter;
// ========== AUTO GENERATED INCLUDE BLOCK END ==========="

void setup() {
  // ===== DO NOT EDIT ANYTHING BELOW THIS LINE =====
  // ========== AUTO GENERATED CODE BEGIN ===========
  // async_tcp:
  // esphome:
  //   name: evamain
  //   platform: ESP8266
  //   board: nodemcu
  //   arduino_version: espressif8266@2.6.2
  //   build_path: evamain
  //   platformio_options: {}
  //   esp8266_restore_from_flash: false
  //   board_flash_mode: dout
  //   includes: []
  //   libraries: []
  App.pre_setup("evamain", __DATE__ ", " __TIME__);
  // binary_sensor:
  // switch:
  // logger:
  //   id: logger_logger
  //   baud_rate: 115200
  //   tx_buffer_size: 512
  //   hardware_uart: UART0
  //   level: DEBUG
  //   logs: {}
  //   esp8266_store_log_strings_in_flash: true
  logger_logger = new logger::Logger(115200, 512, logger::UART_SELECTION_UART0);
  logger_logger->pre_setup();
  App.register_component(logger_logger);
  // web_server_base:
  //   id: web_server_base_webserverbase
  web_server_base_webserverbase = new web_server_base::WebServerBase();
  App.register_component(web_server_base_webserverbase);
  // captive_portal:
  //   id: captive_portal_captiveportal
  //   web_server_base_id: web_server_base_webserverbase
  captive_portal_captiveportal = new captive_portal::CaptivePortal(web_server_base_webserverbase);
  App.register_component(captive_portal_captiveportal);
  // wifi:
  //   ap:
  //     ssid: Evamain Fallback Hotspot
  //     password: 7cfeQbrrzRIi
  //     id: wifi_wifiap
  //     ap_timeout: 1min
  //   id: wifi_wificomponent
  //   domain: .local
  //   reboot_timeout: 15min
  //   power_save_mode: NONE
  //   fast_connect: false
  //   output_power: 20.0
  //   networks:
  //   - ssid: Mai
  //     password: Merchimola1000
  //     id: wifi_wifiap_2
  //     priority: 0.0
  //   use_address: evamain.local
  wifi_wificomponent = new wifi::WiFiComponent();
  wifi_wificomponent->set_use_address("evamain.local");
  wifi::WiFiAP wifi_wifiap_2 = wifi::WiFiAP();
  wifi_wifiap_2.set_ssid("Mai");
  wifi_wifiap_2.set_password("Merchimola1000");
  wifi_wifiap_2.set_priority(0.0f);
  wifi_wificomponent->add_sta(wifi_wifiap_2);
  wifi::WiFiAP wifi_wifiap = wifi::WiFiAP();
  wifi_wifiap.set_ssid("Evamain Fallback Hotspot");
  wifi_wifiap.set_password("7cfeQbrrzRIi");
  wifi_wificomponent->set_ap(wifi_wifiap);
  wifi_wificomponent->set_ap_timeout(60000);
  wifi_wificomponent->set_reboot_timeout(900000);
  wifi_wificomponent->set_power_save_mode(wifi::WIFI_POWER_SAVE_NONE);
  wifi_wificomponent->set_fast_connect(false);
  wifi_wificomponent->set_output_power(20.0f);
  App.register_component(wifi_wificomponent);
  // ota:
  //   id: ota_otacomponent
  //   safe_mode: true
  //   port: 8266
  //   password: ''
  //   reboot_timeout: 5min
  //   num_attempts: 10
  ota_otacomponent = new ota::OTAComponent();
  ota_otacomponent->set_port(8266);
  ota_otacomponent->set_auth_password("");
  App.register_component(ota_otacomponent);
  if (ota_otacomponent->should_enter_safe_mode(10, 300000)) return;
  // api:
  //   id: api_apiserver
  //   port: 6053
  //   password: ''
  //   reboot_timeout: 15min
  api_apiserver = new api::APIServer();
  App.register_component(api_apiserver);
  api_apiserver->set_port(6053);
  api_apiserver->set_password("");
  api_apiserver->set_reboot_timeout(900000);
  // spi:
  //   clk_pin:
  //     number: 16
  //     mode: OUTPUT
  //     inverted: false
  //   miso_pin:
  //     number: 5
  //     mode: INPUT
  //     inverted: false
  //   mosi_pin:
  //     number: 4
  //     mode: OUTPUT
  //     inverted: false
  //   id: spi_spicomponent
  spi_spicomponent = new spi::SPIComponent();
  App.register_component(spi_spicomponent);
  // rc522_spi:
  //   cs_pin:
  //     number: 0
  //     mode: OUTPUT
  //     inverted: false
  //   reset_pin:
  //     number: 2
  //     mode: OUTPUT
  //     inverted: false
  //   update_interval: 1s
  //   id: rc522_spi_rc522spi
  //   spi_id: spi_spicomponent
  rc522_spi_rc522spi = new rc522_spi::RC522Spi();
  rc522_spi_rc522spi->set_update_interval(1000);
  App.register_component(rc522_spi_rc522spi);
  // binary_sensor.rc522:
  //   platform: rc522
  //   uid: 12-E1-C8-49
  //   name: Eva RFID Tag
  //   id: rc522_rc522binarysensor
  //   rc522_id: rc522_spi_rc522spi
  rc522_rc522binarysensor = new rc522::RC522BinarySensor();
  App.register_binary_sensor(rc522_rc522binarysensor);
  rc522_rc522binarysensor->set_name("Eva RFID Tag");
  // binary_sensor.rc522:
  //   platform: rc522
  //   uid: C6-73-2E-1F
  //   name: Ruben RFID Tag
  //   id: rc522_rc522binarysensor_2
  //   rc522_id: rc522_spi_rc522spi
  rc522_rc522binarysensor_2 = new rc522::RC522BinarySensor();
  App.register_binary_sensor(rc522_rc522binarysensor_2);
  rc522_rc522binarysensor_2->set_name("Ruben RFID Tag");
  // binary_sensor.gpio:
  //   platform: gpio
  //   pin:
  //     number: 14
  //     mode: INPUT
  //     inverted: false
  //   name: Botón
  //   device_class: connectivity
  //   id: gpio_gpiobinarysensor
  gpio_gpiobinarysensor = new gpio::GPIOBinarySensor();
  App.register_component(gpio_gpiobinarysensor);
  // binary_sensor.gpio:
  //   platform: gpio
  //   pin:
  //     number: 12
  //     mode: INPUT
  //     inverted: false
  //   name: Sensor
  //   device_class: motion
  //   id: gpio_gpiobinarysensor_2
  gpio_gpiobinarysensor_2 = new gpio::GPIOBinarySensor();
  App.register_component(gpio_gpiobinarysensor_2);
  // binary_sensor.gpio:
  //   platform: gpio
  //   pin:
  //     number: 13
  //     mode: INPUT_PULLUP
  //     inverted: false
  //   name: Puerta
  //   device_class: door
  //   id: puerta_entrada
  //   filters:
  //   - delayed_on_off: 500ms
  //     type_id: binary_sensor_delayedonofffilter
  puerta_entrada = new gpio::GPIOBinarySensor();
  App.register_component(puerta_entrada);
  // switch.gpio:
  //   platform: gpio
  //   pin:
  //     number: 1
  //     mode: OUTPUT
  //     inverted: false
  //   name: LED Verde
  //   id: gpio_gpioswitch
  //   restore_mode: RESTORE_DEFAULT_OFF
  //   interlock_wait_time: 0ms
  gpio_gpioswitch = new gpio::GPIOSwitch();
  App.register_component(gpio_gpioswitch);
  // switch.gpio:
  //   platform: gpio
  //   pin:
  //     number: 15
  //     mode: OUTPUT
  //     inverted: false
  //   name: LED Rojo
  //   id: gpio_gpioswitch_2
  //   restore_mode: RESTORE_DEFAULT_OFF
  //   interlock_wait_time: 0ms
  gpio_gpioswitch_2 = new gpio::GPIOSwitch();
  App.register_component(gpio_gpioswitch_2);
  App.register_binary_sensor(gpio_gpiobinarysensor);
  gpio_gpiobinarysensor->set_name("Bot\303\263n");
  gpio_gpiobinarysensor->set_device_class("connectivity");
  App.register_binary_sensor(gpio_gpiobinarysensor_2);
  gpio_gpiobinarysensor_2->set_name("Sensor");
  gpio_gpiobinarysensor_2->set_device_class("motion");
  App.register_binary_sensor(puerta_entrada);
  puerta_entrada->set_name("Puerta");
  puerta_entrada->set_device_class("door");
  binary_sensor_delayedonofffilter = new binary_sensor::DelayedOnOffFilter(500);
  App.register_component(binary_sensor_delayedonofffilter);
  App.register_switch(gpio_gpioswitch);
  gpio_gpioswitch->set_name("LED Verde");
  App.register_switch(gpio_gpioswitch_2);
  gpio_gpioswitch_2->set_name("LED Rojo");
  spi_spicomponent->set_clk(new GPIOPin(16, OUTPUT, false));
  rc522_spi_rc522spi->set_reset_pin(new GPIOPin(2, OUTPUT, false));
  spi_spicomponent->set_miso(new GPIOPin(5, INPUT, false));
  rc522_spi_rc522spi->register_tag(rc522_rc522binarysensor);
  rc522_rc522binarysensor->set_uid({0x12, 0xE1, 0xC8, 0x49});
  rc522_spi_rc522spi->register_tag(rc522_rc522binarysensor_2);
  rc522_rc522binarysensor_2->set_uid({0xC6, 0x73, 0x2E, 0x1F});
  gpio_gpiobinarysensor->set_pin(new GPIOPin(14, INPUT, false));
  gpio_gpiobinarysensor_2->set_pin(new GPIOPin(12, INPUT, false));
  gpio_gpioswitch->set_pin(new GPIOPin(1, OUTPUT, false));
  gpio_gpioswitch->set_restore_mode(gpio::GPIO_SWITCH_RESTORE_DEFAULT_OFF);
  gpio_gpioswitch_2->set_pin(new GPIOPin(15, OUTPUT, false));
  gpio_gpioswitch_2->set_restore_mode(gpio::GPIO_SWITCH_RESTORE_DEFAULT_OFF);
  spi_spicomponent->set_mosi(new GPIOPin(4, OUTPUT, false));
  rc522_spi_rc522spi->set_spi_parent(spi_spicomponent);
  puerta_entrada->add_filters({binary_sensor_delayedonofffilter});
  rc522_spi_rc522spi->set_cs_pin(new GPIOPin(0, OUTPUT, false));
  puerta_entrada->set_pin(new GPIOPin(13, INPUT_PULLUP, false));
  // =========== AUTO GENERATED CODE END ============
  // ========= YOU CAN EDIT AFTER THIS LINE =========
  App.setup();
}

void loop() {
  App.loop();
}
