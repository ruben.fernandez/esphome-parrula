// Auto generated code by esphome
// ========== AUTO GENERATED INCLUDE BLOCK BEGIN ===========
#include "esphome.h"
using namespace esphome;
using namespace switch_;
using namespace light;
logger::Logger *logger_logger;
web_server_base::WebServerBase *web_server_base_webserverbase;
captive_portal::CaptivePortal *captive_portal_captiveportal;
wifi::WiFiComponent *wifi_wificomponent;
ota::OTAComponent *ota_otacomponent;
api::APIServer *api_apiserver;
using namespace api;
StartupTrigger *startuptrigger;
Automation<> *automation;
gpio::GPIOSwitch *comida;
rgb::RGBLightOutput *rgb_rgblightoutput;
light::LightState *luz;
using namespace output;
esp8266_pwm::ESP8266PWM *output_component1;
esp8266_pwm::ESP8266PWM *output_component2;
esp8266_pwm::ESP8266PWM *output_component3;
light::LightControlAction<> *light_lightcontrolaction;
switch_::TurnOffAction<> *switch__turnoffaction;
// ========== AUTO GENERATED INCLUDE BLOCK END ==========="

void setup() {
  // ===== DO NOT EDIT ANYTHING BELOW THIS LINE =====
  // ========== AUTO GENERATED CODE BEGIN ===========
  // async_tcp:
  // esphome:
  //   name: catfeeder
  //   platform: ESP8266
  //   board: nodemcu
  //   on_boot:
  //   - then:
  //     - light.turn_off:
  //         id: luz
  //         state: false
  //       type_id: light_lightcontrolaction
  //     - switch.turn_off:
  //         id: comida
  //       type_id: switch__turnoffaction
  //     automation_id: automation
  //     trigger_id: startuptrigger
  //     priority: 600.0
  //   arduino_version: platformio/espressif8266@2.6.2
  //   build_path: catfeeder
  //   platformio_options: {}
  //   esp8266_restore_from_flash: false
  //   board_flash_mode: dout
  //   includes: []
  //   libraries: []
  //   name_add_mac_suffix: false
  App.pre_setup("catfeeder", __DATE__ ", " __TIME__, false);
  // switch:
  // light:
  // logger:
  //   id: logger_logger
  //   baud_rate: 115200
  //   tx_buffer_size: 512
  //   hardware_uart: UART0
  //   level: DEBUG
  //   logs: {}
  //   esp8266_store_log_strings_in_flash: true
  logger_logger = new logger::Logger(115200, 512, logger::UART_SELECTION_UART0);
  logger_logger->pre_setup();
  App.register_component(logger_logger);
  // web_server_base:
  //   id: web_server_base_webserverbase
  web_server_base_webserverbase = new web_server_base::WebServerBase();
  App.register_component(web_server_base_webserverbase);
  // captive_portal:
  //   id: captive_portal_captiveportal
  //   web_server_base_id: web_server_base_webserverbase
  captive_portal_captiveportal = new captive_portal::CaptivePortal(web_server_base_webserverbase);
  App.register_component(captive_portal_captiveportal);
  // wifi:
  //   ap:
  //     ssid: Catfeeder Fallback Hotspot
  //     password: ucUYDHAQsDgg
  //     id: wifi_wifiap
  //     ap_timeout: 1min
  //   id: wifi_wificomponent
  //   domain: .local
  //   reboot_timeout: 15min
  //   power_save_mode: NONE
  //   fast_connect: false
  //   output_power: 20.0
  //   networks:
  //   - ssid: Mai
  //     password: Merchimola1000
  //     id: wifi_wifiap_2
  //     priority: 0.0
  //   use_address: catfeeder.local
  wifi_wificomponent = new wifi::WiFiComponent();
  wifi_wificomponent->set_use_address("catfeeder.local");
  wifi::WiFiAP wifi_wifiap_2 = wifi::WiFiAP();
  wifi_wifiap_2.set_ssid("Mai");
  wifi_wifiap_2.set_password("Merchimola1000");
  wifi_wifiap_2.set_priority(0.0f);
  wifi_wificomponent->add_sta(wifi_wifiap_2);
  wifi::WiFiAP wifi_wifiap = wifi::WiFiAP();
  wifi_wifiap.set_ssid("Catfeeder Fallback Hotspot");
  wifi_wifiap.set_password("ucUYDHAQsDgg");
  wifi_wificomponent->set_ap(wifi_wifiap);
  wifi_wificomponent->set_ap_timeout(60000);
  wifi_wificomponent->set_reboot_timeout(900000);
  wifi_wificomponent->set_power_save_mode(wifi::WIFI_POWER_SAVE_NONE);
  wifi_wificomponent->set_fast_connect(false);
  wifi_wificomponent->set_output_power(20.0f);
  App.register_component(wifi_wificomponent);
  // ota:
  //   id: ota_otacomponent
  //   safe_mode: true
  //   port: 8266
  //   password: ''
  //   reboot_timeout: 5min
  //   num_attempts: 10
  ota_otacomponent = new ota::OTAComponent();
  ota_otacomponent->set_port(8266);
  ota_otacomponent->set_auth_password("");
  App.register_component(ota_otacomponent);
  if (ota_otacomponent->should_enter_safe_mode(10, 300000)) return;
  // api:
  //   id: api_apiserver
  //   port: 6053
  //   password: ''
  //   reboot_timeout: 15min
  api_apiserver = new api::APIServer();
  App.register_component(api_apiserver);
  api_apiserver->set_port(6053);
  api_apiserver->set_password("");
  api_apiserver->set_reboot_timeout(900000);
  startuptrigger = new StartupTrigger(600.0f);
  App.register_component(startuptrigger);
  automation = new Automation<>(startuptrigger);
  // switch.gpio:
  //   platform: gpio
  //   name: Comida
  //   pin:
  //     number: 14
  //     mode: OUTPUT
  //     inverted: false
  //   id: comida
  //   restore_mode: RESTORE_DEFAULT_OFF
  //   interlock_wait_time: 0ms
  comida = new gpio::GPIOSwitch();
  App.register_component(comida);
  // light.rgb:
  //   platform: rgb
  //   name: Luz comedero
  //   red: output_component2
  //   green: output_component3
  //   blue: output_component1
  //   id: luz
  //   restore_mode: RESTORE_DEFAULT_OFF
  //   gamma_correct: 2.8
  //   default_transition_length: 1s
  //   output_id: rgb_rgblightoutput
  rgb_rgblightoutput = new rgb::RGBLightOutput();
  luz = new light::LightState("Luz comedero", rgb_rgblightoutput);
  App.register_light(luz);
  App.register_component(luz);
  // output:
  // output.esp8266_pwm:
  //   platform: esp8266_pwm
  //   id: output_component1
  //   pin:
  //     number: 5
  //     mode: OUTPUT
  //     inverted: false
  //   frequency: 1000.0
  output_component1 = new esp8266_pwm::ESP8266PWM();
  App.register_component(output_component1);
  // output.esp8266_pwm:
  //   platform: esp8266_pwm
  //   id: output_component2
  //   pin:
  //     number: 4
  //     mode: OUTPUT
  //     inverted: false
  //   frequency: 1000.0
  output_component2 = new esp8266_pwm::ESP8266PWM();
  App.register_component(output_component2);
  // output.esp8266_pwm:
  //   platform: esp8266_pwm
  //   id: output_component3
  //   pin:
  //     number: 13
  //     mode: OUTPUT
  //     inverted: false
  //   frequency: 1000.0
  output_component3 = new esp8266_pwm::ESP8266PWM();
  App.register_component(output_component3);
  App.register_switch(comida);
  comida->set_name("Comida");
  luz->set_restore_mode(light::LIGHT_RESTORE_DEFAULT_OFF);
  luz->set_default_transition_length(1000);
  luz->set_gamma_correct(2.8f);
  light_lightcontrolaction = new light::LightControlAction<>(luz);
  luz->add_effects({});
  light_lightcontrolaction->set_state(false);
  comida->set_pin(new GPIOPin(14, OUTPUT, false));
  comida->set_restore_mode(gpio::GPIO_SWITCH_RESTORE_DEFAULT_OFF);
  output_component1->set_pin(new GPIOPin(5, OUTPUT, false));
  output_component1->set_frequency(1000.0f);
  output_component2->set_pin(new GPIOPin(4, OUTPUT, false));
  output_component2->set_frequency(1000.0f);
  output_component3->set_pin(new GPIOPin(13, OUTPUT, false));
  output_component3->set_frequency(1000.0f);
  rgb_rgblightoutput->set_red(output_component2);
  switch__turnoffaction = new switch_::TurnOffAction<>(comida);
  rgb_rgblightoutput->set_green(output_component3);
  rgb_rgblightoutput->set_blue(output_component1);
  automation->add_actions({light_lightcontrolaction, switch__turnoffaction});
  // =========== AUTO GENERATED CODE END ============
  // ========= YOU CAN EDIT AFTER THIS LINE =========
  App.setup();
}

void loop() {
  App.loop();
}
